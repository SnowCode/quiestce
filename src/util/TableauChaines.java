package util;

import java.util.Arrays;

public class TableauChaines {
	public static String enChaine(String[] t, String separateur) {
		String resultat = "";
		for (String e: t) {
			resultat += e + separateur;
		}
		resultat = resultat.replaceAll(".$", "");
		return resultat;
	}
	
	public static String plusLongueChaine(String[] t) {
		String max = "";
		for (String chaine: t) {
			if (chaine.length() > max.length()) {
				max = chaine;
			}
		}
		return max;
	}
	
	public static String[] extraireColonne(String[][] t, int indiceColonne) {
		String[] colonne = new String[t.length];
		for (int i = 0; i < t.length; i++) {
			colonne[i] = t[i][indiceColonne];
		}
		return colonne;
	}
	
	public static int[] largeursColonnes(String[] intitules, String[][]	valeurs) {
		String[][] nouveauTableau = new String[valeurs.length + 1][intitules.length];
		nouveauTableau[0] = intitules;
		for (int i = 0; i < valeurs.length; i++) {
			nouveauTableau[i + 1] = valeurs[i];
		}
		
		int[] resultat = new int[intitules.length];
		String[] colonne;
		for (int i = 0; i < intitules.length; i++) {
			colonne = extraireColonne(nouveauTableau, i);
			resultat[i] = plusLongueChaine(colonne).length();
		}
		return resultat;
	}
	
	public static int positionDe(String[] t, String valeur) {
		int resultat = -1;
		for (int i = 0; i < t.length; i++) {
			if (t[i].equalsIgnoreCase(valeur)) {
				resultat = i;
				break;
			}
		}
		return resultat;
	}
	
	public static boolean contient(String[] t, String valeur) {
		boolean resultat = false;
		for (String element: t) {
			if (element != null) {
				if (element.equalsIgnoreCase(valeur)) {
					resultat = true;
					break;
				}
			}
		}
		
		return resultat;
	}
	
	public static String[] valeursDifferentes(String[] t) {
		String[] nouveauT = new String[t.length];
		int compteur = 0;
		for (String element: t) {
			if (!contient(nouveauT, element)) {
				nouveauT[compteur] = element;
				compteur++;
			}
		}
		
		return Arrays.copyOf(nouveauT, compteur);
	}
	
	public static String[][] retirerLignes(String[][] t, int indiceColonne, String valeur, boolean conserverSiEgal) {
		if (t.length > 0) {
			String[][] nouveauT = new String[t.length][t[0].length];
			int compteur = 0;
			for(int i = 0; i < t.length; i++) {
				if (t[i][indiceColonne].equalsIgnoreCase(valeur)) {
					if (conserverSiEgal) {
						nouveauT[compteur] = t[i];
						compteur++;
					} 
				} else if (!conserverSiEgal) {
					nouveauT[compteur] = t[i];
					compteur++;
				}
			}
			return Arrays.copyOf(nouveauT, compteur);
		} else {
			String[][] nouveauT = new String[0][0];
			return nouveauT;
		}
	}
}


