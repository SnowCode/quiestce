package quiestce;

import util.TableauChaines;

public class QuiEstCe {
	public static void main(String[] args) {
		// Variables
		
		// Variables statiques
		String[] caracteristiques = {"PRENOM", "SEXE", "COULEUR DES YEUX", "COULEUR DES CHEVEUX", "LONGUEUR DES CHEVEUX", "LUNETTES", "CHAPEAU", "BOUCLES D'OREILLES", "BARBE", "MOUSTACHE"};
		String[][] personnages = {
				{"Hans", "Homme", "Brun", "Blond", "Court", "Non", "Non", "Non", "Non", "Oui"},
				{"Katrin", "Femme", "Brun", "Brun", "Mi-long", "Non", "Oui", "Non", "Non", "Non"},
				{"Anne", "Femme", "Bleu", "Blanc", "Court", "Oui", "Non", "Oui", "Non", "Non"},
				{"Max", "Homme", "Brun", "Noir", "Court", "Non", "Non", "Non", "Oui", "Non"},
				{"Maria", "Femme", "Brun", "Roux", "Mi-long", "Non", "Oui", "Oui", "Non", "Non"},
				{"Eric", "Homme", "Brun", "Blond", "Court", "Non", "Oui", "Non", "Non", "Non"},
				{"Frank", "Homme", "Brun", "Noir", "Court", "Non", "Oui", "Non", "Non", "Non"},
				{"Lucas", "Homme", "Brun", "Brun", "Court", "Non", "Non", "Non", "Non", "Oui"},
				{"Bernard", "Homme", "Brun", "Brun", "Court", "Non", "Oui", "Non", "Non", "Non"},
				{"Philippe", "Homme", "Brun", "Roux", "Chauve", "Non", "Non", "Non", "Oui", "Oui"},
				{"Roger", "Homme", "Brun", "Brun", "Chauve", "Non", "Non", "Non", "Oui", "Non"},
				{"Paul", "Homme", "Brun", "Blanc", "Court", "Non", "Non", "Non", "Oui", "Non"},
				{"Charles", "Homme", "Bleu", "Roux", "Court", "Oui", "Non", "Non", "Non", "Non"},
				{"Stephen", "Homme", "Brun", "Brun", "Court", "Non", "Non", "Non", "Non", "Oui"},
				{"Victor", "Homme", "Brun", "Blanc", "Mi-long", "Non", "Non", "Non", "Non", "Oui"},
				{"Sophie", "Femme", "Brun", "Noir", "Long", "Oui", "Non", "Oui", "Non", "Non"},
				{"Anita", "Femme", "Bleu", "Blond", "Court", "Non", "Non", "Oui", "Non", "Non"},
				{"Joe", "Homme", "Bleu", "Blond", "Court", "Non", "Non", "Non", "Non", "Non"},
				{"Daniel", "Homme", "Bleu", "Brun", "Chauve", "Oui", "Non", "Non", "Non", "Non"},
				{"Théo", "Homme", "Brun", "Noir", "Court", "Non", "Non", "Non", "Non", "Non"}};
		
		// Variable de départ
		String caracteristique;
		String valeur;
		int positionCaracteristique;
		int questions = 0;
		String[] listeDesValeurs;
		
		// Sélection du personnage mystère
		String[] personnage = personnages[util.Aleatoire.aleatoire(0, personnages.length - 1)];
		
		// Acquisition des données
		while (personnages.length > 1) {
			afficherPersonnages(caracteristiques, personnages);
			
			do  {
				System.out.print("\nCaractéristique ? ");
				caracteristique = io.Console.lireString();
				positionCaracteristique = util.TableauChaines.positionDe(caracteristiques, caracteristique);
			} while (positionCaracteristique == -1);			

			do {
				listeDesValeurs = TableauChaines.valeursDifferentes(TableauChaines.extraireColonne(personnages, positionCaracteristique));
				System.out.printf("\nValeur (%s) ? ", TableauChaines.enChaine(listeDesValeurs, "/"));
				valeur = io.Console.lireString();
			} while (!TableauChaines.contient(listeDesValeurs, valeur));
			
			if (personnage[positionCaracteristique].equalsIgnoreCase(valeur)) {	
				System.out.println("Reponse = Oui");
				personnages = util.TableauChaines.retirerLignes(personnages, positionCaracteristique, valeur, true);
			} else if (caracteristique.equalsIgnoreCase("prenom")){
				System.out.println("Reponse = Non");
				System.out.printf("\nDommage. Le personnage mystère est %s.\n", personnage[util.TableauChaines.positionDe(caracteristiques, "prenom")]);
				break;
			} else {
				System.out.println("Reponse = Non");
				personnages = util.TableauChaines.retirerLignes(personnages, positionCaracteristique, valeur, false);
			}
			
			// Augmenter le compteur des questions
			questions++;
		}
		
		if (personnages.length == 1) {
			System.out.printf("Bravo ! Vous avez trouvé le personnage mystère après %d question(s).\n", questions);
		}
		
		System.out.println("Fin du programme.");
		
		
		
	}
	
	public static void afficherPersonnages(String[] caracteristiques, String[][] personnages) {
		int[] largeursColonnes = util.TableauChaines.largeursColonnes(caracteristiques, personnages);
		System.out.printf("\n%d personnage(s) restant(s) : \n", personnages.length);
		//System.out.println(util.TableauChaines.enChaine(caracteristiques, " | "));
		afficherLigne(caracteristiques, largeursColonnes);
		for (String[] personnage: personnages) {
			//System.out.println(util.TableauChaines.enChaine(personnage, " | "));
			afficherLigne(personnage, largeursColonnes);
		}
	}
	
	public static void afficherLigne(String[] chaines, int[] largeursColonnes) {
		String message = "";
		for (int i = 0; i < chaines.length; i++) {
			message += ajusterLargeur(chaines[i], largeursColonnes[i]) + " | ";
		}
		message = message.replaceAll(" | $", " ");
		System.out.println(message);
	}
	
	/**
	 * Ajuste la longueur d'une chaine de caractère en fonction d'un entier. 
	 * Si la chaine est plus grande que la longueur demadée, elle sera coupée, sinon des espaces seront ajoutés.
	 * 
	 * @param chaine La chaine de caractère à ajuster
	 * @param largeur La longueur de cette chaine
	 * @return la chaine de caractère ajustée
	 */
	public static String ajusterLargeur(String chaine, int largeur) {
		String resultat = "";
		if (chaine.length() > largeur) {
			resultat = chaine.substring(0, largeur);
		} else {
			resultat = chaine + " ".repeat(largeur - chaine.length());
		}
		return resultat;
	}
}
