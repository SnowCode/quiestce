> **Note**: Ce document a été converti en markdown automatiquement avec [pdf2md](https://pdf2md.morethan.io/), elle est approximative. L'original est disponible dans l'espace de cours.

# Examen pratique de janvier 2022

## QUI EST-CE?

« Qui est-ce? » est un jeu de société dans lequel deux joueurs s’affrontent. Chaque joueur possède
les portraits de 20 personnages. Ce sont les mêmes personnages pour chaque joueur.

```
Fig. 1 – Chaque joueur dispose les 20 personnages devant lui.
```

Au début de la partie, chaque joueur choisit secrètement l'un des 20 personnages.

Le but du jeu consiste à identifier le personnage mystère choisi par l'adversaire en posant à tour de
rôle une question sur son apparence physique.

```
Fig. 2 – Apparence du personnage prénommé « Frank ».
```

Il doit être possible de répondre par « oui » ou par « non » à la question posée. Par exemple,

```
− Joueur 1 : « A-t-il un chapeau? »
− Joueur 2 : « Non. »
```

Dans ce cas, le joueur 1 peut logiquement retirer les portraits de tous les personnages portant un
chapeau.

Le premier joueur qui devine correctement l’identité du personnage mystère de l'adversaire
remporte la partie.


### DESCRIPTION DE L’APPLICATION

L’application à réaliser proposera une simplification du jeu décrit ci-dessus.

En effet, un seul joueur sera géré par l’application. Le personnage mystère sera choisi aléatoirement
en début de partie. Ensuite, le joueur doit parvenir à identifier en le moins de questions possibles le
personnage mystère.

## AVANT-PROPOS

À deux exceptions près, les types **String** et **PrintStream** , vous ne pouvez pas utiliser le paradigme
orienté objet pour coder cette application. Déclarez toutes vos fonctions en **static**!

## PRÉPARATION

1. Dans Eclipse, créez un projet Java nommé **B1PRB_Janvier 2022 _NomPrenom** , où
    **NomPrenom** doit être remplacé par vos nom et prénom.

```
N’oubliez pas de configurer l’ encodage des fichiers en UTF- 8!
```
2. Reproduisez exactement l'organisation suivante :

```
Les classes Console et Aleatoire sont disponibles sur l’espace de cours. Les autres classes
doivent être créées par vos soins. La fonction main doit être déclarée dans la classe
QuiEstCe.
```
```
[IMPORTANT] Le projet déposé sur l’espace de cours doit être complet et exécutable!
```

## FONCTIONNALITÉS INDISPENSABLES

Les fonctionnalités suivantes doivent être opérationnelles, tout en veillant à respecter les consignes
données dans leur description :

- Les fonctions _enChaine_ , _positionDe_ et _retirerLignes_ de la classe **TableauChaines** doivent être
    correctement réalisées, testées et utilisées.
- La fonction _afficherPersonnages_ de la classe **QuiEstCe** doit être correctement réalisée et
    utilisée.
- Un tableau de chaînes de caractères à une dimension doit être correctement créé et
    initialisé dans la fonction _main_ de la classe **QuiEstCe** pour répertorier les noms des
    caractéristiques physiques (voir le point A de la section _« Les structures de données »_ ).
- Un tableau de chaînes de caractères à deux dimensions doit être correctement créé et
    initialisé dans la fonction _main_ de la classe **QuiEstCe** pour répertorier les caractéristiques
    physiques des 20 personnages (voir le point B de la section _« Les structures de données »_ ).
- La fonction _main_ doit permettre à l’utilisateur de jouer une partie complète en proposant
    une version simplifiée de l’affichage (voir le premier exemple d’exécution en fin d’énoncé).
    La validation des données saisies par l’utilisateur n’est pas obligatoire. Une partie se termine
    dès que le joueur essaie de deviner le prénom du personnage mystère (autrement dit, c’est
    toujours la dernière question qu’il posera). Une fois la partie terminée, un message
    approprié est affiché : soit _« Bravo! Vous avez trouvé le personnage mystère après 8_
    _question(s). »_ , soit _« Dommage. Le personnage mystère est Frank. »_.
    **[MÉTHODOLOGIE]** En début de partie, aidez-vous de la fonction _aleatoire_ de la classe **Aleatoire**
    pour choisir le personnage mystère. Enregistrez ensuite la référence de la ligne
    correspondante provenant du tableau à deux dimensions afin de pouvoir vérifier à tout
    moment les caractéristiques physiques de ce personnage.

```
[CONSEIL] Durant la réalisation de ce programme, utilisez systématiquement la fonction non
statique equalsIgnoreCase de la classe String au lieu de sa variante equals!
```
**[IMPORTANT]** Si l’une de ces fonctionnalités n’est pas observée, cela entraîne automatiquement
l'échec pour l'examen. Attention, la réussite de ces fonctionnalités seules ne garantit pas l’obtention
d’une note supérieure ou égale à 10 / 20! Soignez vos tests unitaires et ne vous contentez pas de ces
fonctionnalités.

## APPROCHE RECOMMANDÉE

Dans un premier temps, **ne réalisez que les fonctions indispensables**. Ces dernières sont
répertoriées dans la section _« Fonctionnalités indispensables »_ et sont marquées d’un point
d’exclamation au niveau de leurs descriptions.

**Réalisez ensuite la fonction** **_main_** sur base des directives données dans la section _« Fonctionnalités
indispensables »_ et de l’exemple d’exécution **[MINIMUM REQUIS]**.

Il est recommandé de ne réaliser la suite du programme que lorsque ces deux premières étapes sont
terminées.


## LES STRUCTURES DE DONNÉES

```
Pour vous faciliter la tâche dans l’initialisation des structures de données citées ci-dessous, utilisez les
données fournies dans le fichier « Caractéristiques des personnages.txt ».
```
### A. Les noms des caractéristiques physiques

```
Utilisez un tableau de chaînes de caractères à une dimension pour enregistrer les noms des
différentes caractéristiques physiques.
```
```
Voici un aperçu du tableau correspondant qui est à créer et initialiser dans votre fonction main :
```
"PRENOM" "SEXE" (^) DES YEUX""COULEUR
"COULEUR
DES
CHEVEUX"
"LONGUEUR
DES
CHEVEUX"
"LUNETTES" ... ... ... ...

### 0 1 2 3 4 5 6 7 8 9

### B. Les caractéristiques physiques des personnages

```
Utilisez un tableau de chaînes de caractères à deux dimensions pour enregistrer les caractéristiques
physiques des 20 personnages.
Dans ce tableau, chaque ligne correspond à un personnage et chaque colonne est associée à la
caractéristique physique située à la même position au sein du tableau à une dimension.
Voici un aperçu du tableau correspondant qui est à créer et initialiser dans votre fonction main :
```
### 0 "Hans" "Homme" "Brun" "Blond" "Court" "Non" ... ... ... ...

### 1 "Katrin" "Femme" "Brun" "Brun" "Mi-long" "Non" ... ... ... ...

### 2 "Anne"^ "Femme"^ "Bleu"^ "Blanc"^ "Court"^ "Oui"^ ...^ ...^ ...^ ...^

### 3 "Max"^ "Homme"^ "Brun"^ "Noir"^ "Court"^ "Non"^ ...^ ...^ ...^ ...^

### 4 "Maria" "Femme" "Brun" "Roux" "Mi-long" "Non" ... ... ... ...

### ... ... ... ... ... ... ... ... ... ... ...

### 0 1 2 3 4 5 6 7 8 9

### Par exemple, la ligne d’indice 3 répertorie les caractéristiques du personnage prénommé « Max »,

## tandis que la colonne d’indice 2 indique la couleur des yeux de chaque personnage.


## LES CLASSES TABLEAUCHAINES ET TABLEAUCHAINESTESTS

Toutes les fonctions de la classe **TableauChaines** doivent être documentées à l'aide de commentaires
javadoc et doivent être testées à l'aide de JUnit 5.

Dans la classe **TableauChaines** , déclarez :

```
→ Une fonction nommée enChaine qui retourne une chaîne de caractères résultant de la
concaténation de toutes les chaînes provenant d’un tableau tout en intégrant le séparateur
spécifié entre chacune d’elles :
```
```
public static String enChaine(String[] t, String separateur)
```
```
Par exemple, si t est le tableau ["Blond", "Brun", "Blanc", "Noir", "Roux"] et
separateur la chaîne "/", alors la fonction retourne la chaîne
"Blond/Brun/Blanc/Noir/Roux".
```
```
[CAS PARTICULIER] Si t est un tableau vide, alors la fonction retourne une chaîne vide.
```
```
→ Une fonction nommée plusLongueChaine qui retourne la plus longue chaîne de caractères
présente dans un tableau :
```
```
public static String plusLongueChaine(String[] t)
```
```
Par exemple, si t est le tableau ["Court", "Mi-Long", "Long"], alors la fonction
retourne la chaîne "Mi-Long".
[CAS PARTICULIER] Si t est un tableau vide, alors la fonction retourne la référence null.
```
```
→ Une fonction nommée extraireColonne qui retourne un tableau constitué des valeurs
provenant de la colonne d’un tableau à deux dimensions située à l’indice spécifié :
```
```
public static String[] extraireColonne(String[][] t, int indiceColonne)
```
```
Par exemple, si t est le tableau
[["Hans", "Homme", "Court", "Non"],
["Katrin", "Femme", "Mi-Long", "Non"],
["Sophie", "Femme", "Long", "Oui"]]
et indiceColonne l’entier 2 , alors la fonction retourne le tableau ["Court", "Mi-Long",
"Long"].
```
```
[PRÉCISION] Le tableau t est considéré comme « rectangulaire » (autrement dit, ses lignes
possèdent un nombre identique d’éléments).
```
```
[CAS PARTICULIER] Si t est un tableau vide (« constitué de 0 ligne »), alors la fonction retourne
un tableau vide.
```
```
→ Une fonction nommée largeursColonnes qui retourne les largeurs (exprimées en nombre de
caractères) des colonnes d’un tableau sur base de la longueur de la plus grande chaîne
présente dans chaque colonne, et ce compris son intitulé :
```
```
public static int[] largeursColonnes(String[] intitules, String[][]
valeurs)
```

```
Par exemple, si intitules est le tableau ["PRENOM", "SEXE", "LONGUEUR DES
CHEVEUX", "LUNETTES"] et valeurs le tableau
[["Hans", "Homme", "Court", "Non"],
["Katrin", "Femme", "Mi-Long", "Non"],
["Sophie", "Femme", "Long", "Oui"]]
alors la fonction retourne le tableau [6, 5 , 20 , 8 ].
```
```
[CAS PARTICULIER] Si intitules est un tableau vide, alors la fonction retourne un tableau vide.
```
```
[CONTRAINTE] Aidez-vous des fonctions plusLongueChaine et extraireColonne pour réaliser ce
traitement.
```
→ Une fonction nommée _positionDe_ qui retourne la position de la première apparition d’une
chaîne de caractères (en partant de la position 0) au sein d’un tableau :

```
public static int positionDe(String[] t, String valeur)
```
```
Le paramètre valeur indique la chaîne à rechercher.
```
```
[IMPORTANT] Cette fonction ne doit pas tenir compte de la casse des caractères (en anglais,
« case insensitive » ) pour comparer les chaînes.
```
```
Par exemple, si t est le tableau ["PRENOM", "SEXE", "LONGUEUR DES CHEVEUX",
"LUNETTES"] et valeur la chaîne "Longueur des cheveux", alors la fonction retourne
l’entier 2.
```
```
[CAS PARTICULIER] Si la chaîne recherchée n’est pas présente au sein du tableau t, alors la
fonction retourne - 1.
```
```
[SUGGESTION] Aidez-vous de la fonction non statique equalsIgnoreCase de la classe String pour
réaliser ce traitement.
```
→ Une fonction nommée _contient_ qui détermine si une chaîne de caractères est présente dans un
tableau :

```
public static boolean contient(String[] t, String valeur)
```
```
Par exemple, si t est le tableau ["PRENOM", "SEXE", "LONGUEUR DES CHEVEUX",
"LUNETTES"] et valeur la chaîne "Longueur des cheveux", alors la fonction retourne le
booléen true.
```
```
[CONTRAINTE] Aidez-vous de la fonction positionDe pour réaliser ce traitement.
```
→ Une fonction nommée _valeursDifferentes_ qui retourne un tableau dans lequel sont
répertoriées, en un seul exemplaire, chacune des différentes valeurs présentes dans le tableau
spécifié :

```
public static String[] valeursDifferentes(String[] t)
```
```
Par exemple, si t est le tableau ["Noir", "Brun", "Brun", "Roux", "Noir"], alors la
fonction retourne le tableau ["Noir", "Brun", "Roux"].
```

```
[MÉTHODOLOGIE] Suivez la procédure suivante :
```
**1.** Créer un tableau de même longueur que le tableau t afin d’y enregistrer
    ultérieurement des chaînes de caractères provenant de t.
**2.** Parcourir les chaînes du tableau t en prenant soin de ne copier dans le nouveau
    tableau que celles d’entre elles qui n’y sont pas encore présentes.
    **[CONTRAINTE]** Aidez-vous pour ce faire de la fonction _contient_.
**3.** Au terme du traitement précédent, il est fort probable que plusieurs cases du nouveau
    tableau soient inutilisées. Réduire la taille du nouveau tableau de manière à ne
    conserver que les cases utilisées.
    **[SUGGESTION]** Aidez-vous pour ce faire de la fonction _copyOf_ de la classe **Arrays**.

```
[CAS PARTICULIER] Si t est un tableau vide, alors la fonction retourne un tableau vide.
```
→ Une fonction nommée _retirerLignes_ qui copie de manière sélective les lignes d’un tableau à
deux dimensions vers un nouveau tableau :

```
public static String[][] retirerLignes(String[][] t, int
indiceColonne, String valeur, boolean conserverSiEgal)
```
```
Le paramètre indiceColonne indique l’indice de la colonne sur laquelle le critère de
sélection s’applique.
```
```
Le paramètre valeur indique la valeur du critère de sélection.
```
```
Lorsque le paramètre conserverSiEgal est à true, cela signifie qu’une ligne est conservée
à condition que la valeur située dans la colonne indiquée soit identique au paramètre valeur.
```
```
Lorsque le paramètre conserverSiEgal est à false, cela signifie qu’une ligne est conservée
à condition que la valeur située dans la colonne indiquée soit différente du paramètre
valeur.
```
```
[IMPORTANT] Cette fonction ne doit pas tenir compte de la casse des caractères (en anglais,
« case insensitive » ) pour comparer les chaînes.
```
```
Par exemple, si t est le tableau
[["Hans", "Homme", "Court", "Non"],
["Katrin", "Femme", "Mi-Long", "Non"],
["Sophie", "Femme", "Long", "Oui"]]
indiceColonne l’entier 1 , valeur la chaîne "femme" et conserverSiEgal le booléen
true, alors la fonction retourne le tableau
[["Katrin", "Femme", "Mi-Long", "Non"],
["Sophie", "Femme", "Long", "Oui"]]
```
```
Cependant, si le paramètre conserverSiEgal est à false, alors la fonction retourne le
tableau
[["Hans", "Homme", "Court", "Non"]]
```

```
[MÉTHODOLOGIE] Suivez la procédure suivante :
```
**1.** Créer un tableau à deux dimensions de même longueur que le tableau t afin d’y
    enregistrer ultérieurement des références de lignes provenant de t.
**2.** Parcourir les lignes du tableau t en prenant soin de ne copier dans le nouveau tableau
    que celles d’entre elles qui vérifie le critère de sélection.
    **[SUGGESTION]** Aidez-vous pour ce faire de la fonction non statique _equalsIgnoreCase_ de
    la classe **String**.
**3.** Au terme du traitement précédent, il est fort probable que plusieurs cases du nouveau
    tableau soient inutilisées. Réduire la taille du nouveau tableau de manière à ne
    conserver que les cases utilisées.
    **[SUGGESTION]** Aidez-vous pour ce faire de la fonction _copyOf_ de la classe **Arrays**.
**[CAS PARTICULIER]** Si t est un tableau vide (« constitué de 0 ligne ») ou si aucune ligne du tableau
t n’est conservée, alors la fonction retourne un tableau vide.

## LES CLASSES QUIESTCE ET QUIESTCETESTS

Seule la fonction _ajusterLargeur_ de la classe **QuiEstCe** doit être documentée à l'aide d’un
commentaire javadoc et testée à l'aide de JUnit 5.

Dans la classe **QuiEstCe** , déclarez :

```
→ Une fonction nommée ajusterLargeur qui adapte la longueur d’une chaîne de caractères en la
complétant par des caractères espace (symbolisé ici par « ˽ ») ou en réduisant son nombre de
caractères :
```
```
public static String ajusterLargeur(String chaine, int largeur)
```
```
Le paramètre chaine est la chaîne à adapter.
```
```
La paramètre largeur indique la longueur souhaitée.
```
```
Par exemple, si chaine est la chaîne "Long" et largeur l’entier 7 , alors la fonction retourne
la chaîne "Long˽˽˽".
```
```
Cependant, si le paramètre largeur vaut 3 , alors la fonction retourne la chaîne "Lon".
```
```
[SUGGESTION] Aidez-vous des fonctions non-statiques substring et repeat de la classe String
pour réaliser ce traitement.
```
```
→ Une fonction nommée afficherLigne qui affiche en une ligne les chaînes issues d’un tableau
tout en adaptant la largeur occupée par chacune d’elles en fonction des largeurs spécifiées et
en intégrant une barre verticale « | » comme séparateur :
```
```
public static void afficherLigne(String[] chaines, int[]
largeursColonnes)
```
```
Par exemple, si chaines est le tableau ["Katrin", "Femme", "Mi-Long", "Non"] et
largeursColonnes le tableau [6, 4, 10, 4], alors la fonction affiche :
```

```
Katrin˽|˽Femm˽|˽Mi-Long˽˽˽˽|˽Non˽
```
```
[CONTRAINTE] Aidez-vous de la fonction ajusterLargeur pour réaliser ce traitement.
```
```
→ Une fonction nommée afficherPersonnages qui affiche le nombre de personnages présents
dans un tableau à deux dimensions ainsi que l’ensemble de leurs caractéristiques physiques :
```
```
public static void afficherPersonnages(String[] caracteristiques,
String[][] personnages)
```
```
Par exemple, si caracteristiques est le tableau ["PRENOM", "SEXE", "LONGUEUR
DES CHEVEUX", "LUNETTES"] et personnages le tableau
[["Hans", "Homme", "Court", "Non"],
["Katrin", "Femme", "Mi-Long", "Non"],
["Sophie", "Femme", "Long", "Oui"]]
alors la fonction affiche :
[MINIMUM REQUIS] Les données séparées par des barres verticales sans se préoccuper de leur
alignement.
3 personnage(s) restant(s) :
PRENOM | SEXE | LONGUEUR DES CHEVEUX | LUNETTES
Hans | Homme | Court | Non
Katrin | Femme | Mi-Long | Non
Sophie | Femme | Long | Oui
```
```
[CONTRAINTE] Aidez-vous de la fonction enChaine de la classe TableauChaines pour réaliser ce
traitement.
```
```
[VERSION IDÉALE] Les données alignées verticalement.
```
```
3 personnage(s) restant(s) :
PRENOM | SEXE | LONGUEUR DES CHEVEUX | LUNETTES
Hans | Homme | Court | Non
Katrin | Femme | Mi-Long | Non
Sophie | Femme | Long | Oui
```
```
[CONTRAINTE] Aidez-vous de la fonction largeursColonnes de la classe TableauChaines et de la
fonction afficherLigne de la classe QuiEstCe pour réaliser ce traitement.
```
## [MINIMUM REQUIS] EXEMPLE D'EXÉCUTION

```
20 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES CHEVEUX ...
Hans | Homme | Brun | Blond | Court | Non | Non | Non | Non | Oui
Katrin | Femme | Brun | Brun | Mi-long | Non | Oui | Non | Non | Non
Anne | Femme | Bleu | Blanc | Court | Oui | Non | Oui | Non | Non
Max | Homme | Brun | Noir | Court | Non | Non | Non | Oui | Non
Maria | Femme | Brun | Roux | Mi-long | Non | Oui | Oui | Non | Non
Eric | Homme | Brun | Blond | Court | Non | Oui | Non | Non | Non
Frank | Homme | Brun | Noir | Court | Non | Oui | Non | Non | Non
Lucas | Homme | Brun | Brun | Court | Non | Non | Non | Non | Oui
Bernard | Homme | Brun | Brun | Court | Non | Oui | Non | Non | Non
```

```
Philippe | Homme | Brun | Roux | Chauve | Non | Non | Non | Oui | Oui
Roger | Homme | Brun | Brun | Chauve | Non | Non | Non | Oui | Non
Paul | Homme | Brun | Blanc | Court | Non | Non | Non | Oui | Non
Charles | Homme | Bleu | Roux | Court | Oui | Non | Non | Non | Non
Stephen | Homme | Brun | Brun | Court | Non | Non | Non | Non | Oui
Victor | Homme | Brun | Blanc | Mi-long | Non | Non | Non | Non | Oui
Sophie | Femme | Brun | Noir | Long | Oui | Non | Oui | Non | Non
Anita | Femme | Bleu | Blond | Court | Non | Non | Oui | Non | Non
Joe | Homme | Bleu | Blond | Court | Non | Non | Non | Non | Non
Daniel | Homme | Bleu | Brun | Chauve | Oui | Non | Non | Non | Non
Théo | Homme | Brun | Noir | Court | Non | Non | Non | Non | Non
Caractéristique? sexe
Valeur? femme
Réponse = Oui
5 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES CHEVEUX ...
Katrin | Femme | Brun | Brun | Mi-long | Non | Oui | Non | Non | Non
Anne | Femme | Bleu | Blanc | Court | Oui | Non | Oui | Non | Non
Maria | Femme | Brun | Roux | Mi-long | Non | Oui | Oui | Non | Non
Sophie | Femme | Brun | Noir | Long | Oui | Non | Oui | Non | Non
Anita | Femme | Bleu | Blond | Court | Non | Non | Oui | Non | Non
Caractéristique? lunettes
Valeur? non
Réponse = Non
2 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES CHEVEUX ...
Anne | Femme | Bleu | Blanc | Court | Oui | Non | Oui | Non | Non
Sophie | Femme | Brun | Noir | Long | Oui | Non | Oui | Non | Non
Caractéristique? prenom
Valeur? sophie
Réponse = Oui
Bravo! Vous avez trouvé le personnage mystère après 3 question(s).
Fin du programme.
```
## [VERSION IDÉALE] EXEMPLE D'EXÉCUTION

```
20 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES ...
Hans | Homme | Brun | Blond | Court ...
Katrin | Femme | Brun | Brun | Mi-long ...
Anne | Femme | Bleu | Blanc | Court ...
Max | Homme | Brun | Noir | Court ...
Maria | Femme | Brun | Roux | Mi-long ...
Eric | Homme | Brun | Blond | Court ...
Frank | Homme | Brun | Noir | Court ...
Lucas | Homme | Brun | Brun | Court ...
Bernard | Homme | Brun | Brun | Court ...
Philippe | Homme | Brun | Roux | Chauve ...
Roger | Homme | Brun | Brun | Chauve ...
Paul | Homme | Brun | Blanc | Court ...
Charles | Homme | Bleu | Roux | Court ...
Stephen | Homme | Brun | Brun | Court ...
Victor | Homme | Brun | Blanc | Mi-long ...
Sophie | Femme | Brun | Noir | Long ...
Anita | Femme | Bleu | Blond | Court ...
Joe | Homme | Bleu | Blond | Court ...
Daniel | Homme | Bleu | Brun | Chauve ...
Théo | Homme | Brun | Noir | Court ...

Caractéristique? **couleur des cheveu**
Caractéristique? **couleur des cheveux**
Valeur (Blond/Brun/Blanc/Noir/Roux)? **noir**
Réponse = Oui

4 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES CHEVEUX ...
Max | Homme | Brun | Noir | Court ...
Frank | Homme | Brun | Noir | Court ...
Sophie | Femme | Brun | Noir | Long ...
Théo | Homme | Brun | Noir | Court ...

Caractéristique? **chapeau**
Valeur (Non/Oui)? **ouii**
Valeur (Non/Oui)? **oui**
Réponse = Non

3 personnage(s) restant(s) :
PRENOM | SEXE | COULEUR DES YEUX | COULEUR DES CHEVEUX | LONGUEUR DES CHEVEUX ...
Max | Homme | Brun | Noir | Court ...
Sophie | Femme | Brun | Noir | Long ...
Théo | Homme | Brun | Noir | Court ...

Caractéristique? **prenom**
Valeur (Max/Sophie/Théo)? **théo**
Réponse = Non

Dommage. Le personnage mystère est Max.

Fin du programme.
```


