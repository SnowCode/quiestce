package quiestce;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

public class QuiEstCeTests {
	@Test
	public void ajusterLargeurTests() {
		assertEquals(QuiEstCe.ajusterLargeur("Long", 3), "Lon");
		assertEquals(QuiEstCe.ajusterLargeur("Long", 7), "Long   ");
	}
}
