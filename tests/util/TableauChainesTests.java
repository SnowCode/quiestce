package util;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

public class TableauChainesTests {
	@Test
	public void enChaineTests() {
		String[] tableau = { "Blond", "Brun", "Blanc", "Noir", "Roux" };
		assertEquals(TableauChaines.enChaine(tableau, "/"), "Blond/Brun/Blanc/Noir/Roux");
		
		tableau = new String[0];
		assertEquals(TableauChaines.enChaine(tableau, "/"), "");
	}
	
	@Test
	public void plusLongueChaineTests() {
		String[] tableau = {"Court", "Mi-Long", "Long"};
		assertEquals(TableauChaines.plusLongueChaine(tableau), "Mi-Long");
		//tableau = new String[0];
		//assertEquals(TableauChaines.plusLongueChaine(tableau), null);
	}
	
	@Test
	public void extraireColonneTests() {
		String[][] tableau = {{"Hans", "Homme", "Court", "Non"},
							 {"Katrin", "Femme", "Mi-Long", "Non"},
							 {"Sophie", "Femme", "Long", "Oui"}};
		String[] tableauAttendu = {"Court", "Mi-Long", "Long"};
		assertArrayEquals(TableauChaines.extraireColonne(tableau, 2), tableauAttendu);
		
		// Si t est un tableau constituté de 0 lignes
		tableau = new String[0][0];
		tableauAttendu = new String[0];
		assertArrayEquals(tableau, tableauAttendu);
	}
	
	@Test
	public void largeursColonnesTests() {
		String[][] tableau = {{"Hans", "Homme", "Court", "Non"},
				 {"Katrin", "Femme", "Mi-Long", "Non"},
				 {"Sophie", "Femme", "Long", "Oui"}};
		String[] intitules = {"PRENOM", "SEXE", "LONGUEUR DES CHEVEUX", "LUNETTES"};
		int[] tableauAttendu = {6, 5, 20, 8};
		assertArrayEquals(TableauChaines.largeursColonnes(intitules, tableau), tableauAttendu);
	}
	
	@Test
	public void positionDeTests() {
		String[] tableau = {"PRENOM", "SEXE", "LONGUEUR DES CHEVEUX", "LUNETTES"};
		assertEquals(TableauChaines.positionDe(tableau, "LonGueur dEs CheveUx"), 2);
	}
	
	@Test
	public void contientTest() {
		String[] tableau = {"PRENOM", "SEXE", "LONGUEUR DES CHEVEUX", "LUNETTES"};
		assertTrue(TableauChaines.contient(tableau, "Longueur des cheveux"));
	}
	
	@Test
	public void valeursDifferentesTests() {
		String[] tableau = {"Noir", "Brun", "Brun", "Roux", "Noir"};
		String[] tableauAttendu = {"Noir", "Brun", "Roux"};
		assertArrayEquals(TableauChaines.valeursDifferentes(tableau), tableauAttendu);
		
		String[] tableauVide = new String[0];
		assertArrayEquals(TableauChaines.valeursDifferentes(tableauVide), tableauVide);
	}
	
	@Test
	public void retirerLignesTests() {
		String[][] tableau = {{"Hans", "Homme", "Court", "Non"},
				 {"Katrin", "Femme", "Mi-Long", "Non"},
				 {"Sophie", "Femme", "Long", "Oui"}};
		String[][] tableauAttendu = {{"Hans", "Homme", "Court", "Non"}};
		assertArrayEquals(TableauChaines.retirerLignes(tableau, 1, "femme", false), tableauAttendu);
		
		
		String[][] tableauAttendu1 = {{"Katrin", "Femme", "Mi-Long", "Non"},
									{"Sophie", "Femme", "Long", "Oui"}};
		assertArrayEquals(TableauChaines.retirerLignes(tableau, 1, "femme", true), tableauAttendu1);
		
		// Quand aucune occurence de la valeur est trouvée
		String[][] tableauVide = new String[0][0]; 
		assertArrayEquals(TableauChaines.retirerLignes(tableau, 1, "abracadabra", true), tableauVide);
		
		// Quand le tableau est vide
		assertArrayEquals(TableauChaines.retirerLignes(tableauVide, 1, "femme", true), tableauVide);
		
	}
}
